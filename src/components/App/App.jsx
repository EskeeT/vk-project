import { GlobalStyle, Wrapper, Container, H1 } from './styled';
import { Form } from '../Form';

const App = () => {
  return (
    <>
      <GlobalStyle />
      <Wrapper >
        <Container>
          <H1>Форма бронирования переговорной</H1>
          <Form />
        </Container>
      </Wrapper>
    </>
  );
}

export default App;
