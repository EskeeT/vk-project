import styled from 'styled-components';

export const Box = styled.div`
  display: flex;
  margin: 20px;
  justify-content: flex-start;
`;

export const Textarea = styled.textarea`
  width: 700px;
  height: 100px;
  resize: vertical;
  min-height: 50px;
  max-height: 300px;
  border-radius: 5px;
  border: 1px solid #e1e3e6;
  font-size: 16px;
  padding: 10px;
  outline-color: #2684ff;
`;

export const Button = styled.button`
  border: 1px solid #e1e3e6;
  font-size: 20px;
  line-height: 1.5;
  transition: all 150ms ease;
  cursor: pointer;
  border-radius: 5px;
  box-shadow: inset 0px 0px 2px #303234;
  background-color: white;
  height: auto;
  width: auto;
  padding: 8px;

  &:hover {
    border-color: #3fa2ff;
    background-color: #3fa2ff;
    color: white;
  }
  &:focus {
    border: 1px solid #3fa2ff;
  }
  &:active {
    color: white;
    box-shadow: 0px;
    background-color: #2684ff;
    border: 1px solid #2684ff;
  }
  &[disabled] {
    cursor: not-allowed;
    text-shadow: none;
    background-color: white;
    border-color: #e1e3e6;
    color: #e1e3e6;
  }
`;

export const Container = styled.div`
  display: flex;
  margin: 20px;
  width: 100%;
  height: 100%;
  flex-direction: column;
`;

export const ButtonContainer = styled.div`
  display: flex;
  margin: 20px;
  justify-content: flex-end;
  height: 50px;
  width: 700px;
  & > button:first-child {
    margin-right: 60px;
  }
`;
export const DatePickerStyled = styled.input`
  width: 300px;
  padding: 10px;
  font: inherit;
  padding: 7.5px 10px;
  box-sizing: border-box;
  border-radius: 5px;
  border: 1px solid #e1e3e6;
  color: hsl(0, 0%, 20%);
  outline-color: #2684ff;
`;

export const LabelTitle = styled.div`
  margin-bottom: 8px;
`;

export const ErrorContainer = styled.div`
  background-color: #f0b2b2;
  border: 2px solid #e26868;
  width: 720px;
  border-radius: 5px;
  padding: 10px;
  box-sizing: border-box;
`;
