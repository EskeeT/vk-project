import { useState } from 'react';
import { Textarea, Button, Box, Container, ButtonContainer, DatePickerStyled, LabelTitle, ErrorContainer } from './styled'
import Select from 'react-select';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { towerSelect, roomSelect, levelSelect, timeSelect } from './Options'

export const Form = () => {
  const [tower, setTower] = useState('');
  const [level, setLevel] = useState(null);
  const [room, setRoom] = useState(null);
  const [startDate, setStartDate] = useState(new Date());
  const [time, setTime] = useState([]);
  const [comment, setComment] = useState('');



  const checkDate = () => {
    const nowDate = new Date();
    const checkMonth = nowDate.getMonth() < startDate.getMonth();
    const checkDay = nowDate.getDate() <= startDate.getDate();
    const checkYear = nowDate.getFullYear() < startDate.getFullYear();

    if (nowDate.getFullYear() === startDate.getFullYear()) {
      if (nowDate.getMonth() === startDate.getMonth()) {
        if (checkDay) {
          return true;
        } else {
          return false;
        }
      } else if (checkMonth) {
        return true;
      } else {
        return false;
      }
    } else if (checkYear) {
      return true;
    } else {
      return false;
    }
  }

  const disabledSend = !tower || !room || !level || !time.length || !checkDate();

  const handleSend = () => {
    if (disabledSend) return;
    const month = `${(startDate.getMonth() + 1)}`.padStart(2, "0");
    const date = startDate.getDate() + '.' + month + '.' + startDate.getFullYear();
    const timeArray = time.map(({ value }) => value);
    const json = {
      tower: tower.value,
      level: level.value,
      room: room.value,
      date,
      time: timeArray,
      comment
    }
    console.log(json);
    console.log(JSON.stringify(json));
  };

  const handleSendClear = () => {
    setComment('');
    setRoom(null);
    setTower('');
    setLevel(null);
    setStartDate(new Date());
    setTime([]);
  };

  return (
    <Container>
      <Box>
        <label>
          <LabelTitle>Башня</LabelTitle>
          <Select
            styles={{
              control: (baseStyles) => ({
                ...baseStyles, width: 300
              })
            }}
            value={tower}
            options={towerSelect}
            placeholder={'Выберите башню'}
            onChange={(selected) => setTower(selected)}
          />
        </label>
      </Box>
      <Box>
        <label>
          <LabelTitle>Этаж</LabelTitle>
          <Select
            styles={{
              control: (baseStyles) => ({
                ...baseStyles, width: 300
              })
            }}
            value={level}
            options={levelSelect}
            placeholder={'Выберите этаж'}
            onChange={(selected) => setLevel(selected)}
          />
        </label>
      </Box>
      <Box>
        <label>
          <LabelTitle>Переговорная</LabelTitle>
          <Select
            styles={{
              control: (baseStyles) => ({
                ...baseStyles, width: 300
              })
            }}
            options={roomSelect}
            value={room}
            placeholder={'Выберите переговорную'}
            onChange={(selected) => setRoom(selected)}
          />
        </label>
      </Box>
      <Box>
        <label>
          <LabelTitle>Дата</LabelTitle>
          <DatePicker
            customInput={<DatePickerStyled />}
            value={startDate}
            selected={startDate}
            onChange={(date) => setStartDate(date)}
          />
        </label>
      </Box>
      {checkDate() || (
        <Box>
          <ErrorContainer>Ошибка: введите корректную дату</ErrorContainer>
        </Box>
      )}
      <Box>
        <label>
          <LabelTitle>Время</LabelTitle>
          <Select
            styles={{
              control: (baseStyles) => ({
                ...baseStyles, width: 300
              })
            }}
            isMulti
            value={time}
            options={timeSelect}
            placeholder={'Выберите время'}
            onChange={(selected) => setTime(selected)}
          />
        </label>
      </Box>
      <Box>
        <label>
          <LabelTitle>Комментарий</LabelTitle>
          <Textarea
            value={comment}
            onChange={(e) => setComment(e.target.value)}
            placeholder={'Введите комментарий'}
          />
        </label>
      </Box>
      <ButtonContainer>
        <Button onClick={handleSendClear}>Очистить</Button>
        <Button disabled={disabledSend} onClick={handleSend}>Отправить</Button>
      </ButtonContainer>
    </Container >
  );
}
